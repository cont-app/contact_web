import VuexPersistence from 'vuex-persist'

export default ({
  store
}) => {
  window.onNuxtReady(() => {
    new VuexPersistence({
      key: "CMP",
      storage: window.localStorage
    }).plugin(store);
  });
}
