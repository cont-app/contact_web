import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persistedstate";

Vue.use(Vuex);

const createStore = () => {
  return new Vuex.Store({
    state: {
      userId: null,
      contacts: [],
      token: null,
      selectedUser: {
        name: "no data",
        id: "no data",
        email: "no data",
        address: "no data",
        phone_number: "no data"
      },
      userInfo: {},
      postImage: null
    },

    mutations: {
      setImage(state, image) {
        state.postImage = image;
      },

      setToken(state, token) {
        state.token = token;
      },

      setUserInfo(state, data) {
        state.userInfo = data;
      },

      removeToken(state) {
        state.token = null;
      },

      removeUserId(state) {
        state.userId = null;
      },

      removeUserInfo(state) {
        state.userInfo = {}
      },

      removeContacts(state) {
        state.contacts = [];
      },

      setUserId(state, ID) {
        state.userId = ID;
      },

      setContacts(state, contacts) {
        if (contacts) {
          state.contacts = contacts;
        }
      },

      addContact(state, contact) {
        state.contacts.push(contact);
      },

      setSelectedUser(state, contactData) {
        if (contactData) {
          state.selectedUser = contactData;
        }
      },

      deleteContact(state, data) {
        let index = state.contacts.indexOf(data);
        state.contacts.splice(index, 1);
      },

      afterDelete(state) {
        if (state.contacts) {
          state.selectedUser = state.contacts[0];
        }
      },

      updateContact(state, upData) {
        let index = state.contacts.indexOf(state.selectedUser);
        state.contacts.splice(index, 1, upData);
        this.selectedUser = {};
      },

      updateUser(state, usData) {
        state.userInfo = usData;
      },

      favoriteContact(state, favData) {
        state.selectedUser = favData;
        let index = state.contacts.indexOf(favData);
        state.contacts.splice(index, 1, favData);
      }
    },
    actions: {
      search(vCtx, term) {
        if (term && term !== "") {
          let URL = `/contacts/search?id=${this.state.userId}&term=${term}&access_token=${this.state.token}`
          return this.$axios.$get(URL).then(res => {
            vCtx.commit('setContacts', res.contacts);
          })
        } else if (term === "") {
          vCtx.dispatch('getContacts')
        }


      },
      uploadPhoto(vCtx, {
        fd,
        type
      }) {
        let URL = `/uploadPhotos/image/upload?access_token=${this.state.token}`;
        return this.$axios.$post(URL, fd).then(res => {
          let Data;
          if (type === 'user') {
            Data = this.state.userInfo;
            Data.profile_pic = res.result.files.image[0].name;
            vCtx.dispatch('updateUser', Data);
          } else if (type === 'contact') {
            Data = this.state.selectedUser;
            Data.profile_pic = res.result.files.image[0].name;
            vCtx.dispatch('updateContact', Data);
          } else if (type === 'newCont') {
            vCtx.commit('setImage', res.result.files.image[0].name);
          }
        });
      },

      setSelectedUser(vCtx) {
        vCtx.commit("afterDelete");
      },

      favoriteCont(vCtx, favData) {
        console.log("in the store :", favData);

        let URL = `/contacts/favoriteContact?id=${favData.id}&boo=${favData.boo}&access_token=${this.state.token}`;
        return this.$axios
          .$get(URL)
          .then(res => {
            console.log(res);
            let newSelected = this.state.selectedUser;
            newSelected.favorite = !favData.boo;
            vCtx.commit("favoriteContact", newSelected);
          })
          .catch(err => console.log(err));
      },

      updateContact(vCtx, upData) {
        let URL = `/contacts/update?where={"id":"${this.state.selectedUser.id}"}&access_token=${this.state.token}`;
        return this.$axios
          .$post(URL, upData)
          .then(res => {
            let newData = upData;

            if (upData.id === undefined || upData.parent_id === undefined || upData.favorite === undefined || upData.profile_pic === undefined) {
              newData = {
                ...upData,
                id: this.state.selectedUser.id,
                parent_id: this.state.selectedUser.parent_id,
                favorite: this.state.selectedUser.favorite,
                profile_pic: this.state.selectedUser.profile_pic
              }
            }

            vCtx.commit("updateContact", newData);
            vCtx.commit("setSelectedUser", newData);
          })
          .catch(err => console.log(err));
      },

      updateUser(vCtx, usData) {
        let URL = `/appUsers/update?where={"id":"${this.state.userId}"}`;
        return this.$axios
          .$post(URL, usData)
          .then(res => {
            console.log(res);
            vCtx.commit("updateUser", {
              ...usData,
              id: this.state.userInfo.id,
              profile_pic: this.state.userInfo.profile_pic
            });
          })
          .catch(err => console.log(err));
      },

      getContacts(vCtx) {
        let URL = `/contacts/getContact?id=${this.state.userId}&access_token=${this.state.token}`;
        return this.$axios
          .$get(URL)
          .then(res => {
            vCtx.commit("setContacts", res.contact);
            vCtx.commit("setSelectedUser", res.contact[0]);
          })
          .catch(err => console.log(err));
      },

      getUserInfo(vCtx) {
        let URL = `/appUsers/getMyInfo?id=${this.state.userId}`;

        return this.$axios
          .$get(URL)
          .then(res => {
            vCtx.commit("setUserInfo", res.userData[0]);
          })
          .catch(err => {
            console.log("ayseram", err);
          });
      },

      deleteContact(vCtx, sUser) {
        if (this.state.contacts) {
          let URL = `/contacts/${sUser.id}?access_token=${this.state.token}`;
          return this.$axios
            .$delete(URL)
            .then(res => {
              vCtx.commit("deleteContact", sUser);
              vCtx.dispatch("setSelectedUser");
            })
            .catch(err => console.log(err));
        }
      },

      authenticateUser(vCtx, data) {
        let URL = "/appUsers/login";
        return this.$axios
          .$post(URL, {
            email: data.email,
            password: data.password
          })
          .then(res => {
            vCtx.commit("setToken", res.id);
            vCtx.commit("setUserId", res.userId);
            vCtx.dispatch("getContacts");
            vCtx.dispatch("getUserInfo");
          })
          .catch(err => console.log(err));
      },

      createContact(vCtx, contData) {
        let URL =
          "/contacts?access_token=" + this.state.token;
        return this.$axios
          .$post(URL, {
            name: contData.name,
            phone_number: contData.phone,
            email: contData.email,
            address: contData.address,
            parent_id: this.state.userId,
            profile_pic: contData.profile_pic
          })
          .then(res => {
            console.log(res);
            vCtx.commit("addContact", res);
            vCtx.commit("setSelectedUser", res);
          })
          .catch(err => console.log(err));
      },


      logout(vCtx) {
        let URL = `/appUsers/logout?access_token=${this.state.token}`
        this.$axios.$post(URL).then(() => {
          vCtx.commit("removeToken");
          vCtx.commit("removeUserId");
          vCtx.commit("removeContacts");
          vCtx.commit("removeUserInfo");
        })

      },

      singleUser(vCtx, contactData) {
        vCtx.commit("setSelectedUser", contactData);
      }
    },

    getters: {
      getUserInfo(state) {
        return {
          ...state.userInfo,
          contNum: state.contacts.length
        };
      },

      isAuthenticated(state) {
        return state.token != null;
      },

      loadedContacts(state) {
        return state.contacts;
      },

      singleUser(state) {
        return state.selectedUser;
      },

      getToken(state) {
        return state.token;
      },

      getUserId(state) {
        return state.userId;
      },

      getContNum(state) {
        return state.contacts.length;
      },

      getFavCont(state) {
        let favs = [];
        let counter = 0;
        state.contacts.forEach(elt => {
          if (elt.favorite) {
            favs[counter] = elt;
            counter++;
          }
        });
        return favs;
      },

      getToken(state) {
        return state.token;
      },

      getImage(state) {
        return state.postImage;
      }
    },

    plugins: [
      VuexPersist({
        storage: window.sessionStorage
      })
    ]
  });
};

export default createStore;
